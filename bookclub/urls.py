from django.conf.urls.static import static
from django.urls import path

from tutorial import settings
from . import views

urlpatterns = [
    path('', views.all_books, name="all_books"),
    path('book-list', views.book_list, name="book_list"),
    path('book/<int:pk>/', views.book_detail, name="book_detail"),
]